import System.IO

main = do
  putStrLn "please enter filename ...."
  filename <- getLine
  handle <- openFile filename ReadMode
  contents <- hGetContents handle
  putStr (removecomments contents Incode)
  hClose handle

data Flag = Incode | Instring
removecomments :: String -> Int -> String

removecomments [] flag = []
removecomments  ('"' : code) Incode = '"' : (removecomments code Instring)
removecomments  ('"' : code ) Instring = '"': (removecomments code Incode)
removecomments  ('-':'-':code) Incode = removecomments (removeline code) Incode
removecomments  ('{':'-':code) Incode = removecomments (removeblock code) Incode
removecomments  ( x : code) flag = x : (removecomments code flag)

removeline :: String -> String
removeline [] = []
removeline ('\n':xs) = xs
removeline (_:xs) = removeline xs

removeblock :: String -> String
removeblock [] = []
removeblock ('-':'}':xs) = xs
removeblock (_:xs) = removeblock xs
