{-basic language interpreter version 1 , working on another solution using state monad -}

data AExp =  Var Char 
           |Value Int
           |Plus AExp AExp
           |Sub AExp AExp 
           |Mul AExp AExp
           |Div AExp AExp

data BExp =  Vari Char
            |Val Bool 
            |Or Bool Bool
            |And Bool Bool
            |Le AExp AExp
            |Ge AExp AExp
            |Eq AExp AExp

data Statements =  Decl Char 
                  |AAssign Char AExp 
                  |BAssign Char BExp 
                  |While BExp [Statements]
                 
type Language = [Statements]
type Env = [(Char,Int)]


mlookup :: Char -> Env -> Int
mlookup c [] = 0
mlookup c ((x,y):env) | c==x = y 
                      |otherwise = mlookup c env
                   
update :: (Char,Int)  -> Env -> Env
update (vr,v) env = foldr (\(x,y) -> if x==vr then ((vr,v):) else ((x,y):) ) [] env

beval' :: (Int -> Int -> Bool) -> AExp -> AExp -> Env -> Bool
beval' f (Var v1) (Var v2) env = f (mlookup v1 env) (mlookup v2 env)
beval' f (Value v1) (Var v2) env = f (mlookup v1 env) v2
beval'  f (Var v1) (Value v2) env = f v1 (mlookup v2 env)                                    
beval'  f (Value v1) (Value v2) env = f v1 v2                                    

beval :: BExp -> Env -> Bool
beval (Ge e1 e2) env = beval' (>) e1 e2 env
beval (Le e1 e2) env = beval' (<) e1 e2 env
beval (Eq e1 e2) env = beval' (==) e1 e2 env
                                                                        
aeval :: AExp -> Env -> Int
aeval (Var v) env = mlookup v env 
aeval (Value v) env = v
aeval (Plus e1 e2) env = (aeval e1 env) + (aeval e2 env)
aeval (Sub e1 e2) env = (aeval e1 env) - (aeval e2 env)
aeval (Mul e1 e2) env = (aeval e1 env) * (aeval e2 env)
aeval (Div e1 e2) env = div (aeval e1 env)  (aeval e2 env)

interpreter :: Language -> Env -> Env
interpreter [] env = env
interpreter ((Decl vr) : l) env = interpreter  l ((vr,0):env)
interpreter ((AAssign vr v) : l) env = interpreter l (update (vr,aeval v env) env)
interpreter w@((While bexp lexp):l) env = case (beval bexp env) of
                                             True  -> interpreter w (interpreter lexp env)
                                             False -> interpreter l env

run c = interpreter c []
code :: Language
code = [Decl 'C',Decl 'A', Decl 'B' , AAssign  'A' (Value 0) , AAssign 'B' (Value 4) , While (Le (Var 'A') (Var 'B') ) [ AAssign 'A' (Plus (Var 'A') (Value 1)) , AAssign 'C' (Plus (Var 'A') (Value 1))] ]

--to execute
--run code 