import Control.Monad.State
import qualified Data.Map as Map
import Data.Maybe
type Env   = Map.Map Char Int
data AExp = Var Char
            | Value Int
            | Plus AExp AExp
            | Mod AExp AExp
            | Mul AExp AExp
              



aEval :: AExp -> State [String] (Int,Int)
aEval (Value v) = get >>= \xs -> put (("Value"):xs) >> return (v,1) 
aEval (Var k) = get >>= \xs -> put (("Var"):xs) >> return ((fromJust . Map.lookup k $ currEnv) , 1)


aEval (Plus ax1 ax2) =   do
                         
                          (v1,c1) <- aEval ax1
                          (v2,c2) <- aEval ax2
                          modifyy ("plus":) 
                          return (v1 + v2, c1 + c2 + 1)

aEval (Mul ax1 ax2) =   do
                         
                          (v1,c1) <- aEval ax1
                          (v2,c2) <- aEval ax2
                          modifyy ("Mul" :) 
                          return (v1 * v2, c1 + c2 + 1)


modifyy :: ([String] -> [String]) -> State [String] ()
modifyy f = state $ \xs -> ((),f xs)


currEnv = Map.fromList[('X',2),('Y',3),('Z',4)]
expr :: AExp
expr = (Plus (Mul (Var 'X') (Var 'Z')) (Value 3))                      
r :: (t,[String]) -> (t,[String])
r (t,xs) = (t,reverse xs)       

--to execute  
--output format
--((Result,No_of_computations),list of computaion)
-- r$runState (aEval expr) []
