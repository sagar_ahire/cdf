--assignment1
--Sagar Ahire
--roll_no 13102


--find element using foldr

felem1 ::(Eq a)=> a -> [a] -> Bool
felem1 elt [] = False
felem1 elt xs = foldr (||) False (map (elt==) xs) 

--find element using foldl

felem2 ::(Eq a)=> a -> [a] -> Bool
felem2 elt [] = False
felem2 elt xs = foldl (\x y -> x || (elt==y)) False xs


--find element index using foldl

felemi1 :: (Eq a)=> a -> [a] -> (Maybe Int)
felemi1 elt [] = Nothing
felemi1 elt xs = f' ((foldl (\x y -> if y then (fst(x)+1,snd(x)) else f x) (0,0) (map (elt==) xs))) xs
       	      where
		f (x,y) | x == 0 = (x,y+1)  
		       	| otherwise = (x,y)

		f' (x,y) xs | y < length(xs) = Just y
		   	    |otherwise = Nothing

--find element index using foldr

felemi2 :: (Eq a)=> a -> [a] -> (Maybe Int)
felemi2 elt [] = Nothing
felemi2 elt xs = f' ((foldr (\y x -> if y then (fst(x)+1,snd(x)) else f x) (0,0) (map (elt==) xs))) xs
       	      where
	        f (x,y) | x == 0 = (x,y+1)  
  		        | otherwise = (x,y)

		f' (x,y) xs | y < length(xs) = Just (length(xs)-y-1)
		   	    |otherwise = Nothing


--find list to num using foldl
listtonum :: [Int]-> (Maybe Int)
listtonum [] = Nothing
listtonum xs = Just (foldl (\x y->x*10+y) 0 xs)

--find list to num using foldr
listtonum2 :: [Int]-> (Maybe Int)
listtonum2 [] = Nothing
listtonum2 xs = Just (foldr (+) 0 (zipWith (*) xs (reverse [ 10^x | x <-[0..length(xs)-1]])))

--find unzip using foldl 
unzip1 :: [(a,b)] -> ([a],[b])
unzip1 [] = ([],[])
unzip1 xs = foldl f ([],[]) xs
       	  where
		f x y = (fst(x)++[fst(y)],snd(x)++[snd(y)])

--find unzip using foldr 
unzip2 :: [(a,b)] -> ([a],[b])
unzip2 [] = ([],[])
unzip2 xs = foldr f ([],[]) xs
       	  where
		f x y = (fst(x):fst(y),snd(x):snd(y))

--zip

--zip1 :: [a]->[b]->[(a,b)]
--zip1 [] [] = []
--zip1 (x:xs) (y:ys) = (x,y):(zip1 xs ys)


--length using foldl 
len1 :: [a]->Int
len1 [] = 0
len1 xs = foldl f 0 xs
     	where
		f x y = x+1

--length using foldr 
len2 :: [a]->Int
len2 [] = 0
len2 xs = foldr f 0 xs
     	where
		f x y = y+1

--flatten using foldl 
flatten :: [[a]] -> [a]
flatten [] = []
flatten xs = foldl (\x y -> x ++ y ) [] xs

--insert into sorted list using foldl 
insert1 :: (Ord a) => a -> [a] -> [a]
insert1 elt [] = [elt]
insert1 elt xs = f elt (foldl (\x y -> if elt > y then x+1 else x) 0 xs) xs
       	   where
		f elt n l = (take n l) ++ (elt :(drop n l))

--insertion sort
sort1 :: (Ord a) => [a] -> [a]
sort1 [] = []
sort1 xs = foldr insert1 [] xs 


--node deletion from BST

data BSTree a = Empty | Node a (BSTree a) (BSTree a) deriving(Eq,Show)

delete ::(Ord a)=> (BSTree a) -> a -> (BSTree a)
delete Empty node = Empty

delete tree@(Node a Empty Empty) node | a == node = Empty
       	       	     	    	      | otherwise = tree

delete t@(Node a lt Empty) node | a == node = lt
       	          	        | a > node = Node a (delete lt node) Empty
       	       	      	        | otherwise = t 
 
delete (Node a lt (Node b lst rst)) node | a == node = Node b (itl lt lst) rst   
       	       	  	      	    	 | a > node = Node a (delete lt node) (Node b lst rst)
					 | otherwise = Node a lt (delete (Node b lst rst) node)
					 where
						itl st Empty = st
						itl st (Node a Empty Empty) = Node a st Empty
						itl st (Node a Empty rt) = (Node a st rt)
						itl st (Node a lt rt) = (Node a (itl st lt) rt)
						

