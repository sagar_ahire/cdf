                                                                                                         
data Tree a = Leaf a
              | Node a (Tree a) (Tree a)


sumTC (Leaf x) k = k x
sumTC (Node a left right) k = sumTC left contleft
  where contleft sleft = sumTC right contright
          where contright sright = k (a + sleft + sright)

sumTC2 (Leaf x) k = k x
sumTC2 (Node a left right) k = sumTC2 left (\sleft -> sumTC2 right (\sright -> k (a + sleft + sright)))


--tree1 = Node 1 (Node 2 (Leaf 3) (Leaf 4)) (Leaf 5)                                                                                                  

-- -- sumTC2 tree1 id =                                                                                                                               
-- -- sumTC2 (Node 1 (Node 2 (Leaf 3) (Leaf 4)) (Leaf 5)) id                                                                                          

-- -- =sumTC2 (Node 2 (Leaf 3) (Leaf 4)) (\sleft -> sumTC2 (Leaf 5) (\sright -> id (1 + sleft + sright)))                                             
-- -- =sumTC2 (Leaf 3) (\sleft -> sumTC2 (Leaf 4) (\sright -> (\sleft -> sumTC2 (Leaf 5) (\sright -> id (1 + sleft + sright))) ( 2 + sleft + sright)))                                                                                                                                                     
-- -- = (\sleft -> sumTC2 (Leaf 4) (\sright -> (\sleft -> sumTC2 (Leaf 5) (\sright -> id (1 + sleft + sright))) ( 2 + sleft + sright))) 3             
-- -- = sumTC2 (Leaf 4) (\sright -> (\sleft -> sumTC2 (Leaf 5) (\sright -> id (1 + sleft + sright))) ( 2 + 3 + sright))                               
-- -- = (\sright -> (\sleft -> sumTC2 (Leaf 5) (\sright -> id (1 +  sleft + sright))) ( 2 + 3 + sright)) 4                                            
-- -- =  (\sleft -> sumTC2 (Leaf 5) (\sright -> id (1 + sleft + sright))) ( 2 + 3 + 4)                                                                
-- -- =  sumTC2 (Leaf 5) (\sright -> id (1 + ( 2 + 3 + 4) + sright)))                                                                                 
-- -- = (\sright -> id (1 + ( 2 + 3 + 4) + sright))) 5                                                                                                
-- -- = id (1 + ( 2 + 3 + 4) + 5)                                                                                                                     
-- -- = (1 + ( 2 + 3 + 4) + 5)                                                                                                                        
-- -- = (1 + ( 5 + 4) + 5)                                                                                                                            
-- -- = (1 + 9 + 5)                                                                                                                                   
-- -- = (10 + 5)                                                                                                                                      
-- -- = 15                                                                                                                                            




-- --mapT0 f [] k = k []                                                                                                                              
-- len [] k = k 0                                                                                                                                     
-- len (_:xs) k = len xs (\y -> k (y+1))                                                                                                              

-- len [1,2,3,4] id =                                                                                                                                 
--  =len [2,3,4] (\y -> id (y+1))                                                                                                                     
--  =len [3,4] (\y -> (\y -> id (y+1)) (y+1))                                                                                                         
--  =len [4] (\y -> (\y -> (\y -> id (y+1)) (y+1)) (y+1))                                                                                             
--  =len [] (\y -> (\y -> (\y -> (\y -> id (y+1)) (y+1)) (y+1)) (y+1))                                                                                
--  = (\y -> (\y -> (\y -> (\y -> id (y+1)) (y+1)) (y+1)) (y+1)) 0                                                                                    
--  = (\y -> (\y -> (\y -> id (y+1)) (y+1)) (y+1)) (0+1)                                                                                              
--  = (\y -> (\y -> id (y+1)) (y+1)) ((0+1)+1)                                                                                                        
--  = (\y -> id (y+1)) (((0+1)+1)+1)                                                                                                                  
--  = id ((((0+1)+1)+1)+1)                                                                                                                            
--  = (((0+1)+1)+1)+1)                                                                                                                                
--  = (((1+1)+1)+1)                                                                                                                                   
--  = ((2+1)+1)                                                                                                                                       
--  = (3+1)                                                                                                                                           
--  = 4                                                 
