removeduplicates :: Eq a => [a] -> [a] -> [a]
removeduplicates [] l  = reverse l
removeduplicates (x:xs) l | (elem x l) = removeduplicates xs l
                          | otherwise = removeduplicates xs (x:l)


removeadjduplicates :: Eq a => [a] -> [a]
removeadjduplicates []  = []
removeadjduplicates (x:[]) = [x]
removeadjduplicates (x:y:xs)  | x==y = removeadjduplicates xs
                              | otherwise = x : (removeadjduplicates (y:xs))



--removeadjduplicates2 (x:xs) = foldl (\(w,y) z -> if w==z then (z,y) else (z,(w:y))) (x,[]) xs                                                       

--rad (x:xs) = foldl (\x)
