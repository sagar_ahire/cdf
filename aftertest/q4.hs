import Control.Monad.State

--remove duplicates                                                                                                                                   
--v1                                                                                                                                                  
rmdup1 :: Eq a => [a] -> [a]
rmdup1 = reverse.foldl (\z y -> if elem y z then z else (y:z)) []

--v2                                                                                                                                                  
rmdup2 :: Eq a => [a] -> [a]
rmdup2 = foldr (\y z -> if elem y z then z else (y:z)) []

--v3                                                                                                                                                  
rmdup3 :: Eq a => [a] -> [a]
rmdup3 [] = []
rmdup3 (x:xs) = x: (rmdup3 $ filter (x/=) xs)

--v4                                                                                                                                                  
rmdup4 :: Eq a => [a] -> [a]
rmdup4 xs = rmdup4' xs xs id
rmdup4' :: Eq a => [a] -> [a] -> ([a] -> t) -> t
rmdup4' [] xs k  = k xs
rmdup4' (x:xs) xss k = rmdup4' xs xss (\m -> k (x:(filter (x/=) m)))


--v5                                                                                                                                                  
rmdup5 :: Eq a => [a] -> [a]
rmdup5 xs = reverse.snd $ runState (rmdup5' xs) []

rmdup5' :: Eq a => [a] -> State [a] [a]
rmdup5' [] = return []
rmdup5' (x:xs) =  get >>= \ys -> put (x:(filter (x/=) ys)) >> rmdup5' xs
--remove adjacent duplicates                                                                                                                          
--v1                                                                                                                                                  

rmadjdup1 :: Eq a => [a] -> [a]
rmadjdup1 (x:xs) = reverse $ foldl (\z q -> if (head(z)==q) then z else (q:z) ) [x] xs


--v2                                                                                                                                                  
rmadjdup :: Eq a => [a] -> [a]
rmadjdup xs = reverse.snd $ runState (rmadjdup' xs) []

rmadjdup' ::Eq a => [a] -> State [a] undefined
rmadjdup' [] = return undefined
rmadjdup' (x:xs) = get >>= \ys -> put (x:(dropWhile (x==) ys)) >> rmadjdup' xs


--v3                                                                                                                                                  
rmadjdup2 :: Eq a => [a] -> [a]
rmadjdup2 (x:xs) = reverse.snd $ runState (rmadjdup2' xs) [x]

rmadjdup2' ::Eq a => [a] -> State [a] undefined
rmadjdup2' [] = return undefined
rmadjdup2' (x:xs) = get >>= \y -> f y x >> rmadjdup2' xs
                                  where
                                    f y x | (head y) == x = put y
                                          | otherwise = put (x:y)
                                          

--length of longest increasing subsequence

liss :: [Int] -> Int
liss xs = maximum $ map length $ snd (runState (liss2 xs) [])
liss' :: Ord a => [a] -> State [[a]] undefined
liss' [] = return undefined
liss' (x:xs)  = get >>= \st -> put (update x st) >> liss' xs 
                where
                   update x l = [x] : (foldl (\xs ys -> if (head ys)<=x then (x:ys):xs else ys:xs) [] l)                   


